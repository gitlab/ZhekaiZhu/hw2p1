package edu.bu.ec504.hw2.p1.vertices;

import edu.bu.ec504.hw2.p1.support.Weighted;

/**
 * Represents a vertex with an associated weight - analogous to weighted edges.
 * @param <WeightClass>
 */
public class WeightedVertex<WeightClass> extends Vertex  {

  /**
   * Creates a vertex whose weight is assigned to <code>wt</code>.
   * @param wt The weight of the vertex.
   */
  WeightedVertex(WeightClass wt) {
    super();
    myWt = Weighted.of(wt);
  }

  @Override
  public String toString() {
    return super.toString()+" with " + myWt;
  }

  public static <WC> WeightedVertex<WC> of(WC wt) {
    return new WeightedVertex<>(wt);
  }

  // FIELDS
  Weighted<WeightClass> myWt;
}
