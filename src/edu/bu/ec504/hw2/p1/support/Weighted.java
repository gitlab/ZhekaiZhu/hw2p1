package edu.bu.ec504.hw2.p1.support;

/**
 * Adds weight to an object.
 * @param <WeightClass> The class of the weight, for example Integer, Float, or String.
 */
public class Weighted<WeightClass> {

    /**
     * Factory method for producing an object with the given weight.
     * @param wt The weight of the object to be produced.
     * @return An object with the given weight.
     * @param <WC> The class of the weight that is being applied to an object.
     */
    public static <WC> Weighted<WC> of(WC wt) {
        Weighted<WC> newWeight = new Weighted<>();
        newWeight.set(wt);
        return newWeight;
    }

    /**
     * @return The weight associated with this object.
     */
    public WeightClass get() {
        return myWt;
    }

    /**
     * Set this object to the given weight.
     * @param wt The weight to set to the object.
     */
    public void set(WeightClass wt) {
        this.myWt = wt;
    }

    @Override
    public String toString() {
        return "weight ["+myWt+"]";
    }

    /**
     * The weight of the edge.
     */
    WeightClass myWt;
}
