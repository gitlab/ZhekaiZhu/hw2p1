package edu.bu.ec504.hw2.p1.edges.colored_edges;

import edu.bu.ec504.hw2.p1.edges.weighted_edges.WeightedUndirectedEdge;
import edu.bu.ec504.hw2.p1.support.Color;
import edu.bu.ec504.hw2.p1.vertices.Vertex;

public class ColoredUndirectedEdge<VertexClass extends Vertex> extends WeightedUndirectedEdge<VertexClass, Color> {

  /**
   * Creates a weighted undirected edge between <code>source</code> and <code>dest</code>.
   *
   * @param source The initial vertex for the edge.
   * @param dest The final vertex for the edge.
   * @param theClr The color of the edge
   */
  public ColoredUndirectedEdge(VertexClass source, VertexClass dest, Color theClr) {
    super(source, dest, theClr);
  }

  Color getColor() { return myWt.get(); }
}
