package edu.bu.ec504.hw2.p1.edges;

import edu.bu.ec504.hw2.p1.support.UID;
import edu.bu.ec504.hw2.p1.vertices.ColoredVertex;
import edu.bu.ec504.hw2.p1.vertices.Vertex;
import edu.bu.ec504.hw2.p1.vertices.WeightedVertex;

import java.util.ArrayList;

/**
 * A generic edge connecting {@link Vertex} objects.
 * Each edge has a unique ID number.
 * @param <VertexClass> The class of vertices connected by this edge.
 *                     For example, this could be an edge between two
 *                     {@link WeightedVertex}
 *                     or {@link ColoredVertex}
 *                     objects.
 */
public abstract class Edge<VertexClass extends Vertex> extends UID<Edge<VertexClass>> {

    /**
     * @return true if this edge contains vertex V
     */
    public abstract boolean contains(VertexClass V);

    /**
     * @return true iff this edge consists of only the vertiex in Varr and no other vertices.
     */
    public abstract boolean contains(ArrayList<VertexClass> Varr);
}
