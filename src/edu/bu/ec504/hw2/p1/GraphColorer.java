package edu.bu.ec504.hw2.p1;

import edu.bu.ec504.hw2.p1.edges.UndirectedEdge;
import edu.bu.ec504.hw2.p1.graphs.EdgeListGraph;
import edu.bu.ec504.hw2.p1.graphs.Graph;
import edu.bu.ec504.hw2.p1.support.Color;
import edu.bu.ec504.hw2.p1.vertices.ColoredVertex;

/**
 * Attempts to color a graph
 */
public class GraphColorer {

    /**
     * Colors the vertices of an undirected G so that no two adjacent vertices have the same color.
     * @param G The graph to color
     * @modifies The vertex colors of G may be modified by this method.
     * @return The number of different colors used to color the graph.
     */
    static int color(Graph<ColoredVertex, UndirectedEdge<ColoredVertex>> G) {
        for (int ii = 0; ii < G.numVertices(); ii++)
            G.ithVertex(ii).setColor(Color.first());

        return maxColor.ordinal()+1;
    }

    public static void main(String[] args) {
        Graph<ColoredVertex, UndirectedEdge<ColoredVertex>> G = new EdgeListGraph<ColoredVertex, UndirectedEdge<ColoredVertex>>();

        // Build a K_{3,3}
        ColoredVertex[] partA = {
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
        };
        ColoredVertex[] partB = {
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
                ColoredVertex.of(Color.first()),
        };

        // build the graph
        // ... vertices
        for (ColoredVertex v : partA) {
            G.addVertex(v);
        }
        for (ColoredVertex v : partB) {
            G.addVertex(v);
        }
        // ... edges
        for (ColoredVertex v : partA)
            for (ColoredVertex w : partB) {
                G.addEdge(UndirectedEdge.of(v,w));
            }

        // color it
        System.out.println("Colored with: "+color(G)+" colors.");
        System.out.println(G);
    }
}
